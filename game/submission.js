'use strict'; /*jslint node:true*/

/*
 ########################################
 #:+:: O::: O ::::O:   ::: ::::::::O::: #
 #::: :::+:O::: :O  :  :: ::+:: *+*+::O #
 #: +*O  :: +:::++::   : O:+    :: OO:O:#
 #+*:: :::+: +*  +O: +O:*::::+ +:: : : O#
 #::  :+: :+:: ::  *::O*:::: OO  : :O+* #
 #::*O:O :* :*:::: : *:O::** *O : :O:O::#
 # ::  ::+: :: ++::   *: :  :O :::O    O#
 #:+: :+ +  :+:::O :*::::::::O::+:   * :#
 #::+ :  :O:::: ::O::::::+O::  *:::: :  #
 #:: :O:+:O++:* :O+ :::*:  :::::+    *O:#
 #*:+O+: ::**:+:O ::/ + ::O+:*:+:: :::  #
 #  +:::+/  +::::::O  : :+*:  + :::: O:+#
 #OO:+O :  : :  ::: : : +:O :OO :::  +OO#
 #A: : O::::::: :  +:O:O:::*:O:::O+:: ::#
 # :+::O : :  O:  O::::::O:::: :::::: O*#
 #OO:: :*: :::::O: :  ::O: +::: :: +:*: #
 #OOO::O:*::::: :::::O+::::+ O:: O :  O:#
 # :   ::::*:O:   / :O:::: :     :  ::::#
 #*+::  ::  ::+ :*   :::::  : : * :::++ #
 # :O:: O:::: :+:O*:::+ : : O::+:::   ::#
 ########################################
 */

// Objects

const PLAYER = 'A';
const SPACE = ' ';
const DIRT = ':';
const BOULDER = 'O';
const DIAMOND = '*';
const BRICK = '+';
const STEEL = '#';
const BUTTERFLY = ['\\', '|', '/'];

// Moves

const UP = 'u';
const DOWN = 'd';
const RIGHT = 'r';
const LEFT = 'l';

// Strategies

const KILL_BUTTERFLIES = 0;
const COLLECT_DIAMONDS = 1;

const SCREEN_LENGTH = {x: 40, y: 22};

// export for tests
module.exports = {
    PLAYER, SPACE, DIRT, BOULDER, DIAMOND, BRICK, STEEL, BUTTERFLY,
    UP, DOWN, LEFT, RIGHT,
    KILL_BUTTERFLIES, COLLECT_DIAMONDS,
    getMove,
    findObjects,
    isPassable,
    isPushable,
    canMove,
    findPathToObject,
    findPathToKillButterfly,
    findPath,
    sameNode,
    getBackPath,
    getDirection,
    findNeighbours,
    getNextCell,
    getCellObject,
    isDangerous,
    boulderCanFallDown
};

module.exports.play = function*(screen){
    while (true){
        SCREEN_LENGTH.x = screen[0].length;
        SCREEN_LENGTH.y = screen.length;

        yield getMove(screen);
    }
};

function getRandom(arr) {
    return arr.length > 1 ? Math.floor(Math.random()*arr.length) : arr[0];
}

let path = [];

function getMove(screen) {
    let objects = findObjects(screen);
    let strategy = getStrategy(objects);

    if (!hasMove(path)) {
        path = findPath(screen, strategy, objects);
    }

    let move = ' ';

    if (hasMove(path)) {
        move = getDirection(path[0], path[1]);
        path.shift();
    }

    return move;
}

function hasMove(path) {
    return path && path[0] && path[1];
}

function findPath(screen, strategy, objects) {
    switch(strategy) {
        case KILL_BUTTERFLIES:
            return findPathToKillButterfly(screen, objects);
            break;
        case COLLECT_DIAMONDS:
            return findPathToObject(screen, objects);
            break;
    }
}

function findPathToKillButterfly(screen, {butterflies, player}) {
    let target = getRandom(butterflies);

    // TODO: implement
}

function findPathToObject(screen, {diamonds, player}) {
    let checkList = [player];
    let checkedNodes = [];
    let targets = [];

    while(checkList.length > 0) {
        let currentCell = checkList.pop();
        checkedNodes.push(currentCell);

        let neighbours = findNeighbours(screen, currentCell, checkedNodes);
        checkList = [...checkList, ...neighbours];

        targets = diamonds
            .map(cell => neighbours.find(sameNode(cell)))
            .filter(Boolean);

        // stop when find fist target
        if(targets.length) {
            checkList = [];
        }
    }

    return targets.length ? getBackPath(targets[0]) : null;
}

function sameNode(node) {
    return ({x, y}) => node.x === x && node.y === y;
}

function getBackPath(node) {
    let path = [];
    let current = node;

    while(current) {
        let {x, y} = current;
        path.push({x, y});
        current = current.src;
    }

    return path.reverse();
}

function getDirection(from, to) {
    if(from.x === to.x) {
        return to.y > from.y ? DOWN : UP
    }
    else if(from.y === to.y) {
        return to.x > from.x ? RIGHT : LEFT
    }
}

function findNeighbours(screen, src, checkedNodes) {
    return [UP, RIGHT, DOWN, LEFT]
        .map(direction => {
            let cell = getNextCell(direction, src);
            let nextCell = getNextCell(direction, cell);
            return cell && nextCell &&
                canMove(direction, getCellObject(screen, cell), getCellObject(screen, nextCell)) &&
                !isDangerous(screen, direction, src) ?
                Object.assign({}, cell, {src}) : false;
        })
        .filter(Boolean)
        .filter(node => !checkedNodes.some(sameNode(node)))
}

function isDangerous(screen, direction, src) {
    // return false;

    return boulderCanFallDown(screen, direction, src);
}

function boulderCanFallDown(screen, direction, src) {
    let result = false;
    let nextCell, nextObj, upperObj, afterNextObj;

    switch(direction) {
        case DOWN:
            upperObj = getCellObject(screen, getNextCell(UP, src));
            result = [BOULDER, DIAMOND].includes(upperObj);
            break;
        case RIGHT:
        case LEFT:
            nextCell = getNextCell(direction, src);
            nextObj = getCellObject(screen, nextCell);
            upperObj = getCellObject(screen, getNextCell(UP, nextCell));
            result = nextObj === SPACE && [BOULDER, DIAMOND].includes(upperObj);
            break;
        case UP:
            nextCell = getNextCell(direction, src);
            nextObj = getCellObject(screen, nextCell);
            afterNextObj = getCellObject(screen, getNextCell(direction, nextCell));
            result = nextObj === SPACE && [BOULDER, DIAMOND].includes(afterNextObj);
            break;
    }

    // console.log({screen, direction, src, result, nextCell, nextObj, upperObj, afterNextObj});

    return result;
}

function getNextCell(direction, {x, y}) {
    if(!x || !y || x < 0 || x > SCREEN_LENGTH.x || y < 0 || y > SCREEN_LENGTH.y) {
        return null;
    }

    switch(direction) {
        case RIGHT:
            return {x: x + 1, y};
            break;
        case LEFT:
            return {x: x - 1, y};
            break;
        case UP:
            return {x, y: y - 1};
            break;
        case DOWN:
            return {x, y: y + 1};
            break;
    }
}

function getCellObject(screen, {x, y}) {
    return screen && x && y && screen[y] && screen[y][x] || null;
}

function canMove(direction, obj, nextObj) {
    return isPassable(obj) || isPushable(direction, obj, nextObj);
}

function isPassable(obj) {
    return [SPACE, DIRT, DIAMOND].includes(obj)
}

function isPushable(direction, obj, nextObj) {
    return [LEFT, RIGHT].includes(direction) && obj === BOULDER && nextObj === ' '
}

function getStrategy({butterflies, diamonds}) {
    // if(butterflies.length > 0) {
    //     return KILL_BUTTERFLIES
    // }

    if(diamonds.length > 0) {
        return COLLECT_DIAMONDS
    }
}

function findObjects(screen) {
    let butterflies = [];
    let diamonds = [];
    let player = null;

    iterate(screen, (obj, {x, y}) => {
        switch(obj) {
            case BUTTERFLY[0]:
            case BUTTERFLY[1]:
            case BUTTERFLY[2]:
                butterflies.push({x, y});
                break;
            case DIAMOND:
                diamonds.push({x, y});
                break;
            case PLAYER:
                player = {x, y};
                break;
        }
    });

    return {butterflies, diamonds, player}
}

function iterate(screen, callback) {
    for(let y = 0; y < screen.length; y++) {
        let row = screen[y];
        for(let x = 0; x < row.length; x++) {
            callback(row[x], {x, y});
        }
    }
}

/*
TODO:
- check dangerous situations
    - boulder or diamond can fall down from up or from another one
    - butterfly can explode (by one cell to each direction)
    - to be locked
- keep in mind unreachable location
- try to make path at once
- remember previous path and dont use it
- use mark to find the optimal path
 */