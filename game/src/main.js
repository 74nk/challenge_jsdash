import {getMove} from './ai'
import {setScreenScale} from './screen'

export const play = function*(screen){
    while (true){
        setScreenScale(screen);
        yield getMove(screen);
    }
};