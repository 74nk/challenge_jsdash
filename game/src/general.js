// Objects
export const PLAYER = 'A';
export const SPACE = ' ';
export const DIRT = ':';
export const BOULDER = 'O';
export const DIAMOND = '*';
export const BRICK = '+';
export const STEEL = '#';
export const BUTTERFLY = ['\\', '|', '/', '-'];

// Moves
export const UP = 'u';
export const DOWN = 'd';
export const RIGHT = 'r';
export const LEFT = 'l';

// Strategies
export const KILL_BUTTERFLIES = 0;
export const COLLECT_DIAMONDS = 1;
export const WALK_AROUND = 2;