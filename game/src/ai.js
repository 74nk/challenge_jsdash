import {KILL_BUTTERFLIES, COLLECT_DIAMONDS, WALK_AROUND} from './general'

import {findObjects, isDangerous} from './screen'
import {findPath, hasMove, getDirection} from './path'

let path = null;

export function getMove(screen) {
    let objects = findObjects(screen);
    let strategy = getStrategy(objects);

    if (!hasMove(path)) {
        path = findPath(screen, strategy, objects);
    }

    let move = ' ';

    if (hasMove(path)) {
        move = getDirection(path[0], path[1]);
        path.shift();
    }

    if(isDangerous(screen, move, objects.player)) {
        path = null;
        return getMove(screen)
    }

    return move;
}

export function getStrategy({butterflies, diamonds, dirt}) {
    // if(butterflies.length > 0) {
    //     return KILL_BUTTERFLIES
    // }

    if(diamonds.length > 0) {
        return COLLECT_DIAMONDS
    }

    if(dirt.length) {
        return WALK_AROUND
    }
}