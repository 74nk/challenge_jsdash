import {expect} from 'chai'
import {SCREEN, SCREENS} from './utils'
import {getMove} from '../src/ai'
import {setScreenScale} from '../src/screen'

describe('Ai', () => {
    it('1. should get move', () => {
        let move = getMove(SCREEN);
        expect(move).to.equal('l');
    });

    it('2. should get move', () => {
        let screen = SCREENS[5];
        setScreenScale(screen);
        let move = getMove(screen);
        expect(move).to.equal('l');
    });
    
    it('3. should get move', () => {
        let screen = SCREENS[6];
        let move = getMove(screen);
        expect(move).to.equal(' ');
    });
});